package common;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import play.Logger;

import java.util.Properties;
import java.util.Random;
import java.util.concurrent.ExecutionException;

/**
 * Created by Minh NC on 5/25/2017.
 */
public class Producer {

    private static KafkaProducer<Integer, String> producer;

    static {
        init();
    }

    private synchronized static void init() {
        Properties props = new Properties();
        props.put("bootstrap.servers", KafkaProperties.KAFKA_SERVER_URL + ":" + KafkaProperties.KAFKA_SERVER_PORT);
        props.put("client.id", "DemoProducer");
        props.put("key.serializer", "org.apache.kafka.common.serialization.IntegerSerializer");
        props.put("value.serializer", "org.apache.kafka.common.serialization.StringSerializer");
        producer = new KafkaProducer<>(props);
    }

    public static Boolean send(String topic, String message) {
        try {
            Logger.info("sending message  to kafka queue.......");
            producer.send(new ProducerRecord<>(topic, new Random().nextInt(4), message)).get();
            return true;
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
            return  false;
        }
    }

}
