package common;

/**
 * Created by Minh NC on 5/25/2017.
 */
public class KafkaProperties {
    public static final String KAFKA_SERVER_URL = "vnlab-master1.com";
    public static final int KAFKA_SERVER_PORT = 6667;
    public static final int KAFKA_PRODUCER_BUFFER_SIZE = 64 * 1024;
    public static final int CONNECTION_TIMEOUT = 100000;

    private KafkaProperties() {}
}
