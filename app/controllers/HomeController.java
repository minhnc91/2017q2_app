package controllers;

import common.Producer;
import play.Logger;
import play.mvc.Controller;
import play.mvc.Result;
import views.html.index;

import org.apache.commons.lang3.time.FastDateFormat;

import java.util.Random;

/**
 * This controller contains an action to handle HTTP requests
 * to the application's home page.
 */
public class HomeController extends Controller {
    private static final FastDateFormat YYYYMMDDHHMM = FastDateFormat.getInstance("yyyyMMddHHmm");
    private static final String DELIMITER = " ";

    /**
     * An action that renders an HTML page with a welcome message.
     * The configuration in the <code>routes</code> file means that
     * this method will be called when the application receives a
     * <code>GET</code> request with a path of <code>/</code>.
     */
    public Result index() {
        // Kafka
        long t = System.currentTimeMillis();

        String topic = "pageviews_test";
        String msg = YYYYMMDDHHMM.format(t) + DELIMITER + new Random().nextInt(1000)
                + DELIMITER + request().method()
                + DELIMITER + request().uri()
                ;
        if (Producer.send(topic, msg)) {
            Logger.info("OK. topic: " + topic + ", msg: [" + msg + "]");
        }

        return ok(index.render("Web application for test"));
    }

}
