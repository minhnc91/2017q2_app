name := """app-server"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

resolvers += "HDP Maven Repository Releases" at "http://repo.hortonworks.com/content/repositories/releases/"

resolvers += "HDP Maven Repository Public" at "http://repo.hortonworks.com/content/groups/public/"

scalaVersion := "2.11.11"

libraryDependencies += javaJdbc
libraryDependencies += cache
libraryDependencies += javaWs

libraryDependencies += "org.apache.kafka" % "kafka-clients" % "0.10.1.2.6.0.3-8" withSources() withJavadoc()

libraryDependencies += "org.apache.kafka" % "kafka_2.11" % "0.10.1.2.6.0.3-8" withSources() withJavadoc()